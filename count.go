package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

var usage = "usage: %s <file> <number>"
var filePath string
var N int
var err error

func init() {
	if len(os.Args) < 3 {
		log.Fatalf(usage, os.Args[0])
	}
	filePath = os.Args[1]
	N, err = strconv.Atoi(os.Args[2])
	checkErr(err)
}
func main() {

	fh, err := os.Open(filePath)
	defer fh.Close()
	checkErr(err)

	sc := bufio.NewScanner(fh)

	var x float64
	sorted := make([]float64, N) //new array filled with 0s

	for sc.Scan() {
		//read from new number
		x, _ = strconv.ParseFloat(sc.Text(), 64)
		//check if the new number is smaller than the smallest variable in the final array [N-1]
		if x > sorted[N-1] {
			//if true insert the new number to the final array
			for i := 0; i < N; i++ {
				if x >= sorted[i] {
					x, sorted[i] = sorted[i], x
				}
			}
		}
	}

	for _, v := range sorted {
		fmt.Printf("%f\n", v)
	}

	if err := sc.Err(); err != nil {
		log.Fatal(err)
	}
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
